using MlkPwgen;
using System.Net.Http.Headers;
using System.Security.Cryptography;
using System.Text;
using System.Text.Json;
using System.Text.RegularExpressions;

namespace OAuthTest
{
    public partial class Form1 : Form
    {
        private string state;
        private string auth_endpoint;
        private string token_endpoint;
        private string userinfo_endpoint;
        private string client_id;
        private string client_secret;
        private string redirect_uri;
        private string code_verifier;
        private WebViewForm signinWindow;
        private string code_challenge;

        public Form1()
        {
            InitializeComponent();

            // randomowy string
            // state generuje si� raz, a potem jest u�ywany kilkukrotnie
            state = PasswordGenerator.Generate(length: 10, allowed: Sets.Alphanumerics);

            // adresy serwera uwierzytelnienia i tego, kt�ry daje tokeny
            auth_endpoint = "https://login.microsoftonline.com/organizations/oauth2/v2.0/authorize";
            token_endpoint = "https://login.microsoftonline.com/organizations/oauth2/v2.0/token";
            userinfo_endpoint = "https://graph.microsoft.com/oidc/userinfo";

            // by� mo�e musisz wcze�niej zarejestrowa� jako� swoj� aplikacj�, w ka�dym razie powinna zna�
            // te rzeczy i serwer zreszt� te�
            client_id = "4e35bcfc-daec-4bbd-9348-c30d62b39acc";

            // client secret jest u�ywany tylko bez PKCE
            client_secret = "secret";

            // adres na kt�ry zostanie przekierowany u�ytkownik po zalogowaniu si�
            // ja uzywam tutaj mojego uri, ale wiem, ze niektore aplikacje uzywaja http://localhost czy cos takiego
            // nie wiem dobrze czego tutaj u�y�, w gruncie rzeczy chodzi o to aby tlyko wykry� to przekierowanie i wyci�gn��
            // rzeczy z adresu na kt�ry jeste�my przekierowani
            //redirect_uri = "https://ktos.dev";

            // a taki AzureAD dostarcza specjalny link do przekierowa� dla aplikacji natywnych, nawet nie trzeba go rejestrowa�
            redirect_uri = "https://login.microsoftonline.com/common/oauth2/nativeclient";

            // je�eli u�ywamy PKCE to trzeba dodatkowo wygenerowa� losowy code_verifier -- 43 a 128 znak�w
            // alfanumeryczne oraz -._~ s� dopusczalne
            code_verifier = PasswordGenerator.Generate(length: 50, allowed: Sets.Alphanumerics + "-._~");

            // ten code_verifier musi by� dodatkowo potraktowany algorytmem base64url(sha256(code_verifier))
            using var sha256 = SHA256.Create();
            var hash = sha256.ComputeHash(Encoding.UTF8.GetBytes(code_verifier));
            var b64Hash = Convert.ToBase64String(hash);
            code_challenge = Regex.Replace(b64Hash, "\\+", "-");
            code_challenge = Regex.Replace(code_challenge, "\\/", "_");
            code_challenge = Regex.Replace(code_challenge, "=+$", "");
        }

        private void button1_Click(object sender, EventArgs e)
        {
            signinWindow = new WebViewForm();
            signinWindow.OnRedirect += SigninWindow_OnRedirect;

            // https://docs.ifs.com/techdocs/21r2/045_administration_aurena/210_security/040_iam_settings/050_authentication_for_integrations/020_authorization_code_flow/#use-of-oauth2-client-libraries
            //<AUTHORIZATION_ENDPOINT>?response_type=code&client_id=<CLIENT_ID>&state=<STATE>&redirect_uri=<REDIRECT_URI>&nonce=<NONCE>&scope=openid microprofile-jwt

            var nonce = PasswordGenerator.Generate(length: 5, allowed: Sets.Alphanumerics); ; // tutaj randomowy string
            // scope zale�y od twojego API
            var scope = "user.read+openid+profile+email";

            // tutaj mo�e by� wi�cej rzeczy w adresie - jakie� id, zakresy
            var auth_url = $"{auth_endpoint}?response_type=code&client_id={client_id}&state={state}&redirect_uri={redirect_uri}&nonce={nonce}&scope={scope}";

            // https://docs.ifs.com/techdocs/21r2/045_administration_aurena/210_security/040_iam_settings/050_authentication_for_integrations/020_authorization_code_flow/#on-the-use-of-proof-key-for-code-exchange-pkce
            // je�eli u�ywamy PKCE to tutaj si� pojawia ten zaszyfrowany kod
            if (checkBox1.Checked)
                auth_url += $"&code_challenge={code_challenge}&code_challenge_method=S256";
            else
                // je�eli nie u�ywamy PKCE to klient musi mie� sekret i ten sekret
                // musi zna� tak�e serwer je�li dobrze rozumiem
                auth_url += $"&client_secret={client_secret}";

            // za�aduj stron� logowania do okienka
            signinWindow.SetUrl(auth_url);
            signinWindow.ShowDialog();
        }

        // wykrywa przekierowania
        private async void SigninWindow_OnRedirect(object? sender, Uri e)
        {
            // je�eli przekieruje ci� do redirect_uri ze state oraz code w argumentach
            // to znacyz, �e user si� zalogowa� poprawnie i wszystko zadzia�a�o

            // sprawdzam, czy zaczyna si� z redirect_uri, bo on potem dokleja querystringa ze swoimi parametrami
            if (e.AbsoluteUri.StartsWith(redirect_uri))
            {
                var options = e.Query.Substring(1).Split('&');
                var code = options.First(x => x.StartsWith("code=")).Substring(5);
                var state = options.First(x => x.StartsWith("state=")).Substring(6);

                // skoro nas dobrze przekierowa�o to zamykamy okno
                // bo ju� nam nie b�dzie potrzebne
                signinWindow.Close();

                // je�eli state si� zgadza to trzeba wyekstraktowa� code
                if (state == this.state)
                {
                    // tutaj konstruujesz ��danie do token_endpointa
                    var dict = new Dictionary<string, string>();
                    dict.Add("code", code);
                    dict.Add("client_id", client_id);
                    dict.Add("state", state);
                    dict.Add("grant_type", "authorization_code");

                    // m�j testowy serwer tego nie wymaga�, ale ju� AzureAD tak
                    dict.Add("redirect_uri", redirect_uri);

                    // je�li PKCE, to to si� dorzuca tutaj
                    if (checkBox1.Checked)
                        dict.Add("code_verifier", code_verifier);
                    else
                        // je�li nie, to u�ywamy sekretu
                        dict.Add("client_secret", client_secret);

                    var client = new HttpClient();
                    var req = new HttpRequestMessage(HttpMethod.Post, token_endpoint) { Content = new FormUrlEncodedContent(dict) };
                    var res = await client.SendAsync(req);

                    var content = await res.Content.ReadAsStringAsync();

                    if (res.IsSuccessStatusCode)
                    {
                        // odpowied� serwera b�dzie jaka� taka:

                        /*
                        {
                        "access_token":"<INSERT LONG STRING HERE>",
                        "id_token":"<INSERT LONG STRING HERE>",
                        "token_type":"bearer",
                        "expires_in":3600,
                        "refresh_token":"<INSERT LONG STRING HERE>"
                        }
                        */

                        // deserializujemy
                        var t = JsonSerializer.Deserialize<TokenResponse>(await res.Content.ReadAsStringAsync());

                        // i to jest wreszcie ten token, kt�rego mo�emy u�ywa�
                        // do komunikacji z systemem
                        MessageBox.Show($"Token: {t.access_token}");

                        // pr�ba odpytania zabezpieczonego API tokenem uzyskanym z OAuth2
                        client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", t.access_token);

                        var userinfo = await client.GetAsync(userinfo_endpoint);
                        var userinfoContent = await userinfo.Content.ReadAsStringAsync();
                        var userinfoObj = JsonSerializer.Deserialize<UserInfoResponse>(userinfoContent);

                        if (userinfoObj != null)
                            // m�j testowy serwer zwraca tutaj po prostu nazw� u�ytkownika, ale ju� AzureAD jaki� identyfikator tylko
                            MessageBox.Show($"User: {userinfoObj.sub}");
                        else
                            MessageBox.Show("User: null");
                    }
                }
            }
        }
    }

    public class TokenResponse
    {
        public string access_token { get; set; }
        public string id_token { get; set; }
        public string token_type { get; set; }
        public int expires_in { get; set; }
        public string refresh_token { get; set; }
    }

    public class UserInfoResponse
    {
        public string sub { get; set; }
        public string[] aud { get; set; }
        public long nbf { get; set; }
        public string azp { get; set; }
        public string iss { get; set; }
        public long exp { get; set; }
        public long iat { get; set; }
        public string nonce { get; set; }
        public string jti { get; set; }
        public string tid { get; set; }
    }
}