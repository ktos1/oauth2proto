﻿using Microsoft.Web.WebView2.Core;
using Microsoft.Web.WebView2.WinForms;

namespace OAuthTest
{
    public partial class WebViewForm : Form
    {
        public event EventHandler<Uri>? OnRedirect;

        public void SetUrl(string url)
        {
            webView.Source = new Uri(url);
        }

        public WebViewForm()
        {
            InitializeComponent();

            webView.NavigationCompleted += WebView_NavigationCompleted;
        }

        private void WebView_NavigationCompleted(object? sender, Microsoft.Web.WebView2.Core.CoreWebView2NavigationCompletedEventArgs e)
        {
            var w = (sender as WebView2);

            if (w != null)
                OnRedirect?.Invoke(this, w.Source);
        }

        private void WebViewForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            // czyszczenie danych przeglądarki WebView2
            webView.CoreWebView2.Profile.ClearBrowsingDataAsync(CoreWebView2BrowsingDataKinds.AllProfile);
        }
    }
}